FROM python:3.11-slim

RUN mkdir /app
WORKDIR /app
COPY requirements.txt .
RUN pip install -r ./requirements.txt
COPY app.py /app/
CMD ["python", "./app.py"]